import { THOMPSON } from "../constants/people";
import { TEAM_RED, TEAM_BLUE } from "../constants/senders";
import { CLUE_AZZA, CLUE_CAESAR_1, CLUE_SUBST, CLUE_NUMER, CLUE_RAIL, CLUE_COLUMN } from "../constants/sprites";

import AZZACipher from "../ciphers/AZZACipher";
import CaeserCipher from "../ciphers/CaesarCipher";
import SubstitutionCipher from "../ciphers/SubstitutionCipher";
import NumericCipher from "../ciphers/NumericCipher";
import RailFenceCipher from "../ciphers/RailFenceCipher";
import ColumnCipher from "../ciphers/ColumnCipher";

import StringKey from "../keyConfigs/StringKey";
import NumericKey from "../keyConfigs/NumericKey";
import StaticStringKey from "../keyConfigs/StaticStringKey";

const Cae1 = [[CaeserCipher, new NumericKey(1, 1)]];
const Azza = [AZZACipher];
const Subst = [[SubstitutionCipher, new StringKey(3, 9), true]];
const Rail = [[RailFenceCipher, new NumericKey(3, 6), true]];
const Col = [[ColumnCipher, new StringKey(3, 6), true]];
const Numer = [NumericCipher];

const Cae1Numer = [[CaeserCipher, new NumericKey(1, 1)], NumericCipher];
const AzzaNumer = [AZZACipher, NumericCipher];
const SubstNumer = [[SubstitutionCipher, new StringKey(3, 9), true], NumericCipher];
const RailNumer = [[RailFenceCipher, new NumericKey(3, 6), true], NumericCipher];
const ColNumer = [[ColumnCipher, new StringKey(3, 6), true], NumericCipher];

export default {
  cipherList: {
    AZZA: [Azza],
    CAE1: [Cae1],
    RAIL: [Rail],
    COL: [Col],
    RAND: [Rail, Subst, Col],
    NUMER: [Numer],
    RAILNUMER: [RailNumer],
    COLNUMER: [ColNumer],
    RANDNUMER: [RailNumer, SubstNumer, ColNumer],
    RANDONUMER: [Rail, Subst, Col, RailNumer, SubstNumer, ColNumer],
  },
  clues: [CLUE_CAESAR_1, CLUE_AZZA, CLUE_SUBST, CLUE_NUMER, CLUE_RAIL, CLUE_COLUMN],
  messages: [{
    full: `
Tomorrow, we can take down their systems for good. It is as good as done.`,
    sender: TEAM_RED,
    text: "{<COL>Tomorrow it is done}"
  }, {
    full: `
Goodbye tyrants.`,
    sender: TEAM_RED,
    text: "{<COLNUMER>Goodbye tyrants}"
  }]
};