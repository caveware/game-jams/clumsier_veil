// Classes
import BaseState from "./BaseState";

export default class MainState extends BaseState {
  create () {
    console.log("Main state");
  }
}