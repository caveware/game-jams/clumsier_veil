import { DAVIDSON } from "../constants/people";
import { TEAM_RED, TEAM_BLUE } from "../constants/senders";
import { CLUE_AZZA, CLUE_CAESAR_1, CLUE_SUBST, CLUE_NUMER, CLUE_RAIL } from "../constants/sprites";

import AZZACipher from "../ciphers/AZZACipher";
import CaeserCipher from "../ciphers/CaesarCipher";
import SubstitutionCipher from "../ciphers/SubstitutionCipher";
import NumericCipher from "../ciphers/NumericCipher";
import RailFenceCipher from "../ciphers/RailFenceCipher";

import StringKey from "../keyConfigs/StringKey";
import NumericKey from "../keyConfigs/NumericKey";
import StaticStringKey from "../keyConfigs/StaticStringKey";

const Cae1 = [[CaeserCipher, new NumericKey(1, 1)]];
const Azza = [AZZACipher];
const Subst = [[SubstitutionCipher, new StringKey(3, 9), true]];
const Rail = [[RailFenceCipher, new NumericKey(3, 6), true]];
const Numer = [NumericCipher];

const Cae1Numer = [[CaeserCipher, new NumericKey(1, 1)], NumericCipher];
const AzzaNumer = [AZZACipher, NumericCipher];
const SubstNumer = [[SubstitutionCipher, new StringKey(3, 9), true], NumericCipher];
const RailNumer = [[RailFenceCipher, new NumericKey(3, 6), true], NumericCipher];

export default {
  cipherList: {
    AZZA: [Azza],
    CAE1: [Cae1],
    RAIL: [Rail],
    RAND: [Rail, Subst],
    NUMER: [Numer],
    RAILNUMER: [RailNumer],
    RANDNUMER: [RailNumer, SubstNumer],
    RANDONUMER: [Rail, Subst, RailNumer, SubstNumer]
  },
  clues: [CLUE_CAESAR_1, CLUE_AZZA, CLUE_SUBST, CLUE_NUMER, CLUE_RAIL],
  email: {
    content: `
If you cannot accomodate us, you will accommodate nothing. Your country is finished. You have abused us, the people, for too long.
It is time to take back what you took from us.`,
    sender: DAVIDSON,
    title: "Your fate has been sealed"
  },
  messages: [{
    full: `
Are you sure this is the best idea?`,
    sender: TEAM_BLUE,
    text: "{<RAND>Are you sure}"
  }, {
    full: `
Once we start this, it will be a matter of hours before the country is crippled.`,
    sender: TEAM_RED,
    text: "{<RAIL>Once we start}"
  }, {
    full: `
The only way they will listen to us is if we give them no other choice.`,
    sender: TEAM_RED,
    text: "{<RAILNUMER>Its the only way}"
  }]
};