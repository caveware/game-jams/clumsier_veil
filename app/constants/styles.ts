export const CLUE_TEXT = {
  align: "center",
  fill: "#333333",
  font: "32px SM1"
};

export const CALENDAR_STYLE = {
  align: "left",
  fill: "#ffffff",
  font: "20px SM1"
};

export const EMAIL_NAME_STYLE = {
  align: "left",
  fill: "#333333",
  font: "20px SM1"
};

export const EMAIL_ROLE_STYLE = {
  align: "left",
  fill: "#333333",
  font: "18px SM1"
};

export const EMAIL_TEXT_STYLE = {
  align: "left",
  fill: "#333333",
  font: "14px SM1"
};

export const EMAIL_TITLE_STYLE = {
  align: "left",
  fill: "#333333",
  font: "16px SM1"
};

export const GENERAL_INPUT = {
  align: "center",
  fill: "#222222",
  font: "32px LCD"
};

export const NOTE_TEXT = {
  align: "center",
  fill: "#dddddd",
  font: "24px SM1"
};

export const TICKER = {
  align: "center",
  fill: "#22ee22",
  font: "54px LCD"
};

export const TICKER_SYSTEM = {
  align: "center",
  fill: "#cccccc",
  font: "54px LCD",
  highlight: "#cccccc"
};

export const TICKER_TEAM_BLUE = {
  align: "center",
  fill: "#2233ff",
  font: "54px LCD",
  highlight: "#11ddff"
};

export const TICKER_TEAM_RED = {
  align: "center",
  fill: "#cc3311",
  font: "54px LCD",
  highlight: "#ff8833"
};

export const TICKER_TEAM_GREEN = {
  align: "center",
  fill: "#44ff44",
  font: "54px LCD",
  highlight: "#88ff88"
};

export const TICKER_TEAM_PINK = {
  align: "center",
  fill: "#ff44ff",
  font: "54px LCD",
  highlight: "#ff88ff"
};

export const TICKER_TEAM_ORANGE = {
  align: "center",
  fill: "#ffdd44",
  font: "54px LCD",
  highlight: "#ffee88"
};

export const TICKER_TEAM_WHITE = {
  align: "center",
  fill: "#ffffff",
  font: "54px LCD",
  highlight: "#dddddd"
};