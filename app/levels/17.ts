import { } from "../constants/people";
import { } from "../constants/senders";
import { CLUE_AZZA, CLUE_CAESAR_1, CLUE_SUBST } from "../constants/sprites";

export default {
  cipherList: { },
  clues: [CLUE_CAESAR_1, CLUE_AZZA, CLUE_SUBST],
  messages: [ ]
};