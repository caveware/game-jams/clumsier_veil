import KeyConfig from "../interfaces/KeyConfig";
import Dictionary from "../classes/Dictionary";

export default class StringKey implements KeyConfig {

  keyType = "string";

  private dict = new Dictionary;

  constructor (public minLength: number, public maxLength: number) {
    if (minLength > maxLength) {
      let tmp = minLength;
      minLength = maxLength;
      maxLength = tmp;
    }
  }

  generateKey() {
    return this.dict.getWordInLenRange(this.minLength, this.maxLength).toUpperCase();
  }

}