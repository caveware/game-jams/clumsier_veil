import { tween } from "popmotion";

// Classes
import BaseState from "./BaseState";

// Constants
import { SFX_CLICK } from "../constants/audio";
import { DESK_STATE } from "../constants/states";
import {
  FADE,
  START_CONTINUE,
  START_NEW_GAME,
  START_OKAY
} from "../constants/sprites";

export default class SaveState extends BaseState {
  fade: Phaser.Sprite;

  create () {
    const key = localStorage.getItem("CURRENT_LEVEL");
    let info = "This game uses an autosave system.";

    if (key) {
      info = "Autosave found. Load it?";
      const newGame = this.add.sprite(227, 320, START_NEW_GAME);
      newGame.anchor.set(.5);
      newGame.inputEnabled = true;
      newGame.input.useHandCursor = true;
      newGame.events.onInputDown.add(() => {
        this.goToGame(0);
      });
      const cont = this.add.sprite(627, 320, START_CONTINUE);
      cont.anchor.set(.5);
      cont.inputEnabled = true;
      cont.input.useHandCursor = true;
      cont.events.onInputDown.add(() => {
        this.goToGame(+key);
      });
    } else {
      const okay = this.add.sprite(427, 320, START_OKAY);
      okay.anchor.set(.5);
      okay.inputEnabled = true;
      okay.input.useHandCursor = true;
      okay.events.onInputDown.add(() => {
        this.goToGame(0);
      });
    }

    const text = this.add.text(427, 150, info, {
      align: "center",
      fill: "#ffffff",
      font: "32px LCD"
    });
    text.anchor.set(.5);

    this.fade = this.add.sprite(0, 0, FADE);

    tween({
      from: 1,
      to: 0
    }).start({
      update: (value) => {
        this.fade.alpha = value;
      }
    });
  }

  goToGame (level: number) {
    this.sound.play(SFX_CLICK);

    tween({
      from: 0,
      to: 1
    }).start({
      complete: () => {
        this.state.start(DESK_STATE, true, false, level);
      },
      update: (value) => {
        this.fade.alpha = value;
      }
    });
  }

  shutdown () {
    this.sound.stopAll();
  }
}
