import { DAVIDSON } from "../constants/people";
import { DESK_CORKBOARD_REBEL } from "../constants/sprites";

export default {
  cipherList: { },
  clues: [],
  corkboard: DESK_CORKBOARD_REBEL,
  email: {
    content: `
WE FINALLY DID IT.
It may not have been the outcome we were hoping for in the beginning, but we still proved the point that we won't be walked over by people with power.
It will take time for Utopia to be rebuilt, but it will be rebuilt even better,
Thankyou, decipherer.
You saved us.`,
    sender: DAVIDSON,
    title: "WE DID IT!"
  },
  messages: []
};
