import { DESK_TICKER } from "../constants/sprites";
import * as STYLE from "../constants/styles";

interface Options {
  onComplete: Function;
  stopText?: boolean;
}

export default class Ticker extends Phaser.Sprite {
  displayText: string;
  onComplete: Function;
  scroll: boolean = false;
  styleObj: {
    fill: string;
    highlight: string;
  };
  text: Phaser.Text;
  textIndex: number;

  constructor (scene: Phaser.Game, sender: string, private origText: string = "", private diffText: string = "", options: Options) {
    super(scene, 0, 0, DESK_TICKER);
    this.anchor.set(.5);
    scene.stage.addChild(this);

    this.onComplete = options.onComplete;

    this.textIndex = 0;
    this.setStyle(sender);

    this.text = new Phaser.Text(scene, 0, 1, this.getDisplayText(), this.styleObj);
    this.text.anchor.set(.5, 0);
    this.text.position.y = -24;

    this.addChild(this.text);
  }

  changeIndex (index) {
    this.textIndex = index;
    this.text.text = this.getDisplayText();

    const diffText = (" ".repeat(16) + this.diffText).slice(index, 16 + index);
    const origText = (" ".repeat(16) + this.origText).slice(index, 16 + index);

    if (this.isSubstitutionCipher(origText, diffText)) {
      this.text.clearColors();

      let splitDiff = diffText.split("_");
      let splitText = origText.split("_");
      const ids = this.getUnderscoreIds(origText);
      let index = 0;
      let uKey = 0;
      if (ids[0] === 0) {
        this.text.addColor(this.styleObj.fill, 0);
        index = 1;
        uKey = 1;
        splitDiff = splitDiff.slice(1);
        splitText = splitText.slice(1);
      }

      splitText.forEach((phrase, key) => {
        const colour = (phrase !== splitDiff[key])
          ? this.styleObj.highlight
          : this.styleObj.fill;
        this.text.addColor(colour, index);

        const id = ids[uKey++];
        if (id) {
          this.text.addColor(this.styleObj.fill, id);
          index = id + 1;
        }
      });
    }
  }

  getDisplayText () {
    const paddedText = " ".repeat(16) + this.origText;
    let str = "";
    for (let i = 0; i < 16; i++) {
      str += paddedText[(i + this.textIndex) % paddedText.length];
    }
    return str;
  }

  getUnderscoreIds (source: string) {
    const indexes = [];
    source.split("").forEach((_, key) => {
      if (_ === "_") indexes.push(key);
    });
    return indexes;
  }

  isSubstitutionCipher (text: string, diffText: string) {
    const diffIds = this.getUnderscoreIds(diffText);
    const textIds = this.getUnderscoreIds(text);

    return (diffIds.length === textIds.length) && diffIds.reduce((val, id, key) => {
      if (id !== textIds[key]) {
        return false;
      } else {
        return val;
      }
    }, true);
  }

  pause () {
    this.scroll = false;
  }

  setStyle (style: string) {
    this.styleObj = STYLE[`TICKER_${style}`];

    if (this.text) {
      this.text.setStyle(this.styleObj);
    }
  }

  setText (origText: string, diffText?: string) {
    this.origText = origText;
    this.diffText = diffText || origText;
    this.changeIndex(0);
  }

  start () {
    this.scroll = true;
  }

  stop () {
    this.changeIndex(0);
    this.pause();
  }

  updateTicker (dt) {
    if (this.scroll) {
      const oldIndex = Math.floor(this.textIndex);
      this.textIndex += dt * 11;
      if (oldIndex < Math.floor(this.textIndex)) {
        const newIndex = Math.floor(this.textIndex) % (" ".repeat(16) + this.origText).length;
        if (newIndex < oldIndex) this.onComplete();
        this.changeIndex(newIndex);
      }
    }
  }
}
