import { MCLURE } from "../constants/people";
import { TEAM_BLUE, TEAM_RED } from "../constants/senders";
import { CLUE_AZZA, CLUE_CAESAR_1 } from "../constants/sprites";

import AZZACipher from "../ciphers/AZZACipher";

const AZZAOnly = [AZZACipher];

export default {
  cipherList: {
    AZZA: [AZZAOnly]
  },
  clues: [CLUE_AZZA, CLUE_CAESAR_1],
  email: {
    content: `In order to test this system to a greater level, I am typing out a relatively large email (large for testing and this purpose, obviously - don't fight me).

How are you doing today? Alright? That's crazy, I am the same thing!`,
    sender: MCLURE,
    title: "Welcome!"
  },
  messages: [{
    full: `
    Hello, this is McLure. I am here.
    `,
    sender: TEAM_RED,
    text: "HELLO_THIS_IS_{<AZZA>MCLURE}"
  }, {
    full: `cum`,
    sender: TEAM_BLUE,
    text: "ANOTHER_{<AZZA>TEST}_DUDE"
  }]
};