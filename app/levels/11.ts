import { MCLURE } from "../constants/people";
import { TEAM_RED, TEAM_BLUE } from "../constants/senders";
import { CLUE_CAESAR_1, CLUE_AZZA } from "../constants/sprites";

import CaeserCipher from "../ciphers/CaesarCipher";
import NumericKey from "../keyConfigs/NumericKey";
import AZZACipher from "../ciphers/AZZACipher";

const Cae1 = [[CaeserCipher, new NumericKey(1, 1)]];
const Azza = [AZZACipher];

export default {
  cipherList: {
    RAND1: [Cae1, Azza],
    RAND2: [Cae1, Azza],
    RAND3: [Cae1, Azza],
  },
  clues: [CLUE_CAESAR_1, CLUE_AZZA],
  messages: [{
    full: `
URGENT!`,
    sender: TEAM_BLUE,
    text: "_{<RAND1>urgent}_"
  }, {
    full: `
Government snipers found in outskirts of mill!`,
    sender: TEAM_BLUE,
    text: "{<RAND2>Government snipers found}"
  }, {
    full: `
ABORT!`,
    sender: TEAM_BLUE,
    text: "_{<RAND3>abort}_."
  }]
};