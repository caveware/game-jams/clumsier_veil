import { SILVERSTONE } from "../constants/people";
import { TEAM_GREEN } from "../constants/senders";
import { DESK_CORKBOARD_REBEL, CLUE_AZZA, CLUE_CAESAR_1, CLUE_SUBST, CLUE_NUMER, CLUE_RAIL, CLUE_COLUMN } from "../constants/sprites";

import AZZACipher from "../ciphers/AZZACipher";
import CaeserCipher from "../ciphers/CaesarCipher";
import SubstitutionCipher from "../ciphers/SubstitutionCipher";
import NumericCipher from "../ciphers/NumericCipher";
import RailFenceCipher from "../ciphers/RailFenceCipher";
import ColumnCipher from "../ciphers/ColumnCipher";

import StringKey from "../keyConfigs/StringKey";
import NumericKey from "../keyConfigs/NumericKey";
import StaticStringKey from "../keyConfigs/StaticStringKey";

const Cae1 = [[CaeserCipher, new NumericKey(1, 1)]];
const Azza = [AZZACipher];
const Subst = [[SubstitutionCipher, new StringKey(3, 9), true]];
const Rail = [[RailFenceCipher, new NumericKey(3, 6), true]];
const Col = [[ColumnCipher, new StringKey(3, 6), true]];
const Numer = [NumericCipher];

const Cae1Numer = [[CaeserCipher, new NumericKey(1, 1)], NumericCipher];
const AzzaNumer = [AZZACipher, NumericCipher];
const SubstNumer = [[SubstitutionCipher, new StringKey(3, 9), true], NumericCipher];
const RailNumer = [[RailFenceCipher, new NumericKey(3, 6), true], NumericCipher];
const ColNumer = [[ColumnCipher, new StringKey(3, 6), true], NumericCipher];

export default {
  cipherList: {
    AZZA: [Azza],
    CAE1: [Cae1],
    RAIL: [Rail],
    COL: [Col],
    RAND: [Rail, Subst, Col],
    NUMER: [Numer],
    RAILNUMER: [RailNumer],
    COLNUMER: [ColNumer],
    RANDNUMER: [RailNumer, SubstNumer, ColNumer],
    RANDONUMER: [Rail, Subst, Col, RailNumer, SubstNumer, ColNumer],
  },
  clues: [CLUE_CAESAR_1, CLUE_AZZA, CLUE_SUBST, CLUE_NUMER, CLUE_RAIL, CLUE_COLUMN],
  corkboard: DESK_CORKBOARD_REBEL,
  email: {
    content: `
The worst bit is, that story is pleasant compared to how they treated some of us. Everyone in the Exiles was "too dangerous" to live in Utopia. Anyone who even showed the slightest inkling of resisting leadership was branded a traiter and sentenced to "death". Except instead of death, they meant involuntary medical testing of whatever they felt like. They love to boast that the found a cure for cancer, but that "cure" killed tens of thousands of people. Some of us managed to escape, but not many. And there was nothing we could do to get these experiences to the people inside, because if there was any trace that we were alive, we would be hunted down. We had no food, living in run down building left over from the old world, just tryingour best to survive. But we are all that is left now.`,
    sender: SILVERSTONE,
    title: "Just for the record"
  },
  messages: [{
    full: `
Start using the emergency ciphers. He can't be allowed to find our weak point.`,
    sender: TEAM_GREEN,
    text: "{<RANDONUMER>Start emergency ciphers}"
  }]
};
