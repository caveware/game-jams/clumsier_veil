import SubstitutionCipher from "./SubstitutionCipher";
import { ALPHABET } from "../constants/ciphers";
import StaticStringKey from "../keyConfigs/StaticStringKey";
import NumericKey from "../keyConfigs/NumericKey";

export default class CaesarCipher extends SubstitutionCipher {
  distance: number;
  constructor (distanceGen: NumericKey) {
    let idx = distanceGen.generateKey();
    super(new StaticStringKey(ALPHABET.substring(idx) + ALPHABET.substring(0, idx)));
    this.distance = idx;
  }
}