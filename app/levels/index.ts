import LEVEL_01 from "./01";
import LEVEL_02 from "./02";
import LEVEL_03 from "./03";
import LEVEL_04 from "./04";
import LEVEL_05 from "./05";
import LEVEL_06 from "./06";
import LEVEL_07 from "./07";
import LEVEL_08 from "./08";
import LEVEL_09 from "./09";
import LEVEL_10 from "./10";
import LEVEL_11 from "./11";
import LEVEL_12 from "./12";
import LEVEL_13 from "./13";
import LEVEL_14 from "./14";
import LEVEL_15 from "./15";
import LEVEL_16 from "./16";
import LEVEL_17 from "./17";
import LEVEL_18 from "./18";
import LEVEL_19 from "./19";
import LEVEL_20 from "./20";
import LEVEL_21 from "./21";
import LEVEL_22 from "./22";
import LEVEL_23 from "./23";
import LEVEL_24 from "./24";
import LEVEL_25 from "./25";
import LEVEL_26 from "./26";
import LEVEL_27 from "./27";
import LEVEL_28 from "./28";
import LEVEL_29 from "./29";
import LEVEL_30 from "./30";
import LEVEL_31 from "./31";
import G_00 from "./g00";
import G_01 from "./g01";
import G_02 from "./g02";
import G_03 from "./g03";
import G_04 from "./g04";
import G_05 from "./g05";
import G_06 from "./g06";
import G_07 from "./g07";
import R_00 from "./r00";
import R_01 from "./r01";
import R_02 from "./r02";
import R_03 from "./r03";
import R_04 from "./r04";
import R_05 from "./r05";
import R_06 from "./r06";

export default [
  LEVEL_01,
  LEVEL_02,
  LEVEL_03,
  LEVEL_04,
  LEVEL_05,
  LEVEL_06,
  LEVEL_07,
  LEVEL_08,
  LEVEL_09,
  LEVEL_10,
  LEVEL_11,
  LEVEL_12,
  LEVEL_13,
  LEVEL_14,
  LEVEL_15,
  LEVEL_16,
  LEVEL_17,
  LEVEL_18,
  LEVEL_19,
  LEVEL_20,
  LEVEL_21,
  LEVEL_22,
  LEVEL_23,
  LEVEL_24,
  LEVEL_25,
  LEVEL_26,
  LEVEL_27,
  LEVEL_28,
  LEVEL_29,
  LEVEL_30,
  LEVEL_31,
  G_00,
  G_01,
  G_02,
  G_03,
  G_04,
  G_05,
  G_06,
  G_07,
  R_00,
  R_01,
  R_02,
  R_03,
  R_04,
  R_05,
  R_06
];