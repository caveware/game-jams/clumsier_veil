// Classes
import CreditsState from "../states/CreditsState";
import DeskState from "../states/DeskState";
import LogoState from "../states/LogoState";
import MainState from "../states/MainState";
import PreloadState from "../states/PreloadState";
import SaveState from "../states/SaveState";
import SplashState from "../states/SplashState";

// Constants
import { BGM_LIST, BGM_PATH, SFX_LIST, SFX_PATH } from "../constants/audio";
import { GAME_WIDTH, GAME_HEIGHT } from "../constants/display";
import * as KEY from "../constants/keys";
import { SPRITES_LIST, SPRITES_PATH } from "../constants/sprites";
import {
  CREDITS_STATE,
  DESK_STATE,
  MAIN_STATE,
  LOGO_STATE,
  PRELOAD_STATE,
  SAVE_STATE,
  SPLASH_STATE
} from "../constants/states";

export default class Stage extends Phaser.Game {
  constructor () {
    // Call super function
    super(GAME_WIDTH, GAME_HEIGHT, Phaser.AUTO, "game", {
      preload: function () {
        // Use nearest neighbour filtering
        Phaser.Canvas.setImageRenderingCrisp(this.game.canvas);
        this.game.renderer.renderSession.roundPixels = true;

        // Add states
        this.state.add(CREDITS_STATE, CreditsState, false);
        this.state.add(DESK_STATE, DeskState, false);
        this.state.add(LOGO_STATE, LogoState, false);
        this.state.add(MAIN_STATE, MainState, false);
        this.state.add(PRELOAD_STATE, PreloadState, false);
        this.state.add(SAVE_STATE, SaveState, false);
        this.state.add(SPLASH_STATE, SplashState, false);

        // Setup scaling
        this.scale.scaleMode = 2;

        window.onkeydown = (evt) => {
          // Determine key code
          const code = evt.which || evt.keyCode;
          const state = this.state.getCurrentState();

          switch (state.key) {
            case DESK_STATE:
              // Check if in alphabet
              if (code >= KEY.A && code <= KEY.Z) {
                (state as DeskState).addLetter(evt.key.toUpperCase());
              } else if (code === KEY.SPACE || code === KEY.UNDERSCORE_HYPHEN) {
                (state as DeskState).addLetter("_");
              } else if (code === KEY.BACKSPACE) {
                (state as DeskState).removeLetter();
              } else if (code === KEY.ENTER) {
                (state as DeskState).pressEnter();
              } else if (code === 34) {
                (state as DeskState).progress();
              }
              break;
          }
        };
      },
      create: function () {
        this.load.path = "./assets/";

        this.add.text(20, GAME_HEIGHT - 40, "Loading...", {
          fill: "#ffffff"
        });

        this.load.onLoadComplete.addOnce(() => {
          // Start default state
          this.ready = true;
        });

        // Load bgm
        Object.keys(BGM_LIST).forEach((key) => {
          this.load.audio(key, BGM_PATH + BGM_LIST[key]);
        });

        // Load sfx
        Object.keys(SFX_LIST).forEach((key) => {
          this.load.audio(key, SFX_PATH + SFX_LIST[key]);
        });

        // Load sprites
        Object.keys(SPRITES_LIST).forEach((key) => {
          this.load.image(key, SPRITES_PATH + SPRITES_LIST[key]);
        });

        this.load.start();

        this.stage.disableVisibilityChange = true;
      },
      update () {
        if (this.ready) {
          this.state.start(SPLASH_STATE, true, false, 0);
        }
      }
    });
  }
}