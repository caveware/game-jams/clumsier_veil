import { ALPHABET } from "../constants/ciphers";
import Cipher from "../interfaces/Cipher";
import KeyConfig from "../interfaces/KeyConfig";
import StringKey from "../keyConfigs/StringKey";

import { normaliseString } from "../helpers/cipher";

export default class SubstitutionCipher implements Cipher {
  decodeList: object;
  encodeList: object;

  hideClue = true;

  keyword: string;

  constructor (public keyConf: StringKey = new StringKey(2, 12)) {

    this.keyword = keyConf.generateKey();

    // Provide a fix for duplicate letters for STUPID engineers
    let keymap = this.keyword.split("").filter(function(item, pos, self) {
      return self.indexOf(item) === pos;
    }).join("").toUpperCase();

    // Add unused letters of alphabet to keyword
    keymap += ALPHABET.split("").reduce((text, letter) => {
      if (keymap.indexOf(letter) === -1) text += letter;
      return text;
    }, "");

    // Build decode and encode lists
    this.decodeList = {};
    this.encodeList = {};
    keymap.split("").forEach((encodedLetter, key) => {
      const decodedLetter = ALPHABET[key];
      this.decodeList[decodedLetter] = encodedLetter;
      this.encodeList[encodedLetter] = decodedLetter;
    });

    this.decodeList["_"] = "_";
    this.encodeList["_"] = "_";
  }

  decode (text: string) {
    return normaliseString(text).toUpperCase().split("").map((char) => this.decodeList[char]).join("");
  }

  encode (text: string) {
    return normaliseString(text).toUpperCase().split("").map((char) => this.encodeList[char]).join("");
  }
}