import { MCLURE } from "../constants/people";
import { TEAM_BLUE } from "../constants/senders";
import { CLUE_AZZA, CLUE_CAESAR_1, CLUE_SUBST, CLUE_NUMER } from "../constants/sprites";

import AZZACipher from "../ciphers/AZZACipher";
import CaeserCipher from "../ciphers/CaesarCipher";
import SubstitutionCipher from "../ciphers/SubstitutionCipher";
import NumericCipher from "../ciphers/NumericCipher";

import StringKey from "../keyConfigs/StringKey";
import NumericKey from "../keyConfigs/NumericKey";
import StaticStringKey from "../keyConfigs/StaticStringKey";

const Cae1 = [[CaeserCipher, new NumericKey(1, 1)]];
const Azza = [AZZACipher];
const Subst = [[SubstitutionCipher, new StringKey(3, 6), true]];
const Numer = [NumericCipher];

const Cae1Numer = [[CaeserCipher, new NumericKey(1, 1)], NumericCipher];
const AzzaNumer = [AZZACipher, NumericCipher];
const SubstNumer = [[SubstitutionCipher, new StringKey(3, 6), true], NumericCipher];

export default {
  cipherList: {
    RAND: [Azza, Cae1, Subst],
    NUMER: [Numer],
    RANDNUMER: [AzzaNumer, Cae1Numer, SubstNumer],
  },
  clues: [CLUE_CAESAR_1, CLUE_AZZA, CLUE_SUBST, CLUE_NUMER],
  email: {
    content: `
Just a heads up, sometimes there is more than one cipher combined. If you are pretty sure you know what you know what it is, but it doesn't seem to still make sense, maybe there is a second cipher.`,
    sender: MCLURE,
    title: "Double ciphers"
  },
  messages: [{
    full: `
I hope you know what you are doing. We can't afford you to ruin this chance.`,
    sender: TEAM_BLUE,
    text: "{<RAND>know} what you are {<RANDNUMER>doing}"
  }, {
    full: `
We won't get another one.`,
    sender: TEAM_BLUE,
    text: "{<NUMER>We won't get {<RAND>another one}}"
  }]
};