import { MCLURE } from "../constants/people";
import { TEAM_BLUE, TEAM_RED } from "../constants/senders";

export default {
  cipherList: {},
  clues: [],
  email: {
    content: `
Welcome to the Anti-Rebellion Division. Ironically enough, Utopia is always under attack by people who don't want the best for society. It is our job to find out their plans and stop them before it's too late.
Any messages we intercept will be played on the ticker in front of you. All you need to do is transcribe them onto the paper in front of you. For technical reasons, spaces have had to be replaced by underscores in all transcription circumstances.
Good luck.`,
    sender: MCLURE,
    title: "Welcome!"
  },
  messages: [{
    full: `
We have heard some interesting rumours. Like that you want to destroy Utopia.`,
    sender: TEAM_RED,
    text: "We have heard rumours"
  }, {
    full: `
Why would we ever want to do anything against such a "perfect" institution?`,
    sender: TEAM_BLUE,
    text: "Why would we"
  }, {
    full: `
We may be able to help you.`,
    sender: TEAM_RED,
    text: "We may be able to help you"
  }]
};