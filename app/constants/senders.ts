export const CARMEL = "CARMEL";
export const CEYLON = "CEYLON";
export const EVA = "EVA";
export const JASMIN = "JASMIN";
export const MCLURE = "MCLURE";
export const THOMPSON = "THOMPSON";
export const TOM = "TOM";

export const TEAM_BLUE = "TEAM_BLUE";
export const TEAM_RED = "TEAM_RED";
export const TEAM_GREEN = "TEAM_GREEN";
export const TEAM_PINK = "TEAM_PINK";
export const TEAM_ORANGE = "TEAM_ORANGE";
export const TEAM_WHITE = "TEAM_WHITE";