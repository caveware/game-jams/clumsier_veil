import { MCLURE } from "../constants/people";
import { TEAM_BLUE } from "../constants/senders";
import { CLUE_AZZA, CLUE_CAESAR_1, CLUE_SUBST, CLUE_NUMER } from "../constants/sprites";

import AZZACipher from "../ciphers/AZZACipher";
import CaeserCipher from "../ciphers/CaesarCipher";
import SubstitutionCipher from "../ciphers/SubstitutionCipher";
import NumericCipher from "../ciphers/NumericCipher";

import StringKey from "../keyConfigs/StringKey";
import NumericKey from "../keyConfigs/NumericKey";
import StaticStringKey from "../keyConfigs/StaticStringKey";

const Cae1 = [[CaeserCipher, new NumericKey(1, 1)]];
const Azza = [AZZACipher];
const Subst = [[SubstitutionCipher, new StringKey(3, 9), true]];
const Numer = [NumericCipher];

const Cae1Numer = [[CaeserCipher, new NumericKey(1, 1)], NumericCipher];
const AzzaNumer = [AZZACipher, NumericCipher];
const SubstNumer = [[SubstitutionCipher, new StringKey(3, 9), true], NumericCipher];

export default {
  cipherList: {
    RAND: [Azza, Cae1, Subst],
    NUMER: [Numer],
    RANDNUMER: [AzzaNumer, Cae1Numer, SubstNumer],
    RANDONUMER: [Azza, Cae1, Subst, AzzaNumer, Cae1Numer, SubstNumer]
  },
  clues: [CLUE_CAESAR_1, CLUE_AZZA, CLUE_SUBST, CLUE_NUMER],
  email: {
    content: `
I have heard rumours around the office of weakness in the infrastructure. That the rebels have any chance of even making a blemish on Utopia. They do not.
We have power, resources, an army. They have a bunch of maligned ideals, and people who are too weak to fight for them.`,
    sender: MCLURE,
    title: "Weakness"
  },
  messages: [{
    full: `
I wonder if this will work. Can we get inside their heads?`,
    sender: TEAM_BLUE,
    text: "I {<RANDNUMER>wonder} if this will {<RANDNUMER>work}"
  }, {
    full: `
Can we actually do it.`,
    sender: TEAM_BLUE,
    text: "{<RANDONUMER>Can we actually do it}"
  }]
};