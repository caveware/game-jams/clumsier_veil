import { timeline } from "popmotion";

// Classes
import BaseState from "./BaseState";

// Constants
import { SAVE_STATE } from "../constants/states";
import { LOGO_IMAGE } from "../constants/sprites";

export default class LogoState extends BaseState {
  create () {
    const sprite = this.add.sprite(427, 240, LOGO_IMAGE);
    sprite.alpha = 0;
    sprite.anchor.set(.5);

    timeline([
      { track: "fadeIn", from: 0, to: 1, duration: 500 },
      1500,
      { track: "fadeOut", from: 0, to: 1, duration: 500 }
    ]).start({
      complete: () => {
        this.game.state.start(SAVE_STATE);
      },
      update: (data) => {
        sprite.alpha = data.fadeIn - data.fadeOut;
      }
    });
  }
}
