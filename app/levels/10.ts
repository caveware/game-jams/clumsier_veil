import { MCLURE } from "../constants/people";
import { TEAM_RED, TEAM_BLUE } from "../constants/senders";
import { CLUE_CAESAR_1, CLUE_AZZA } from "../constants/sprites";

import CaeserCipher from "../ciphers/CaesarCipher";
import NumericKey from "../keyConfigs/NumericKey";
import AZZACipher from "../ciphers/AZZACipher";

const Cae1 = [[CaeserCipher, new NumericKey(1, 1)]];
const Azza = [AZZACipher];

export default {
  cipherList: {
    RAND: [Cae1, Azza]
  },
  clues: [CLUE_CAESAR_1, CLUE_AZZA],
  email: {
    content: `
It looks like we may have a chance to kill this rebellion before it even starts. Thanks to your information, we have a company of troops surrounding the mill, ready for the rebels to show up so we can eradicate them like the weeds they are in the garden of Utopia. And it is all thanks to your work here. Without you, they could have escaped us. But you've personally taken the safety off of the rifle aimed for their vitals. Good work.
I will be watching over this operation personally, so just continue the good work until I am back.`,
    sender: MCLURE,
    title: "This could be our chance"
  },
  messages: [{
    full: `
I know we shouldnt be talking about this while the bosses are meeting up, but what if they've pushed too quickly.`,
    sender: TEAM_RED,
    text: "What if this is too {<RAND>soon}"
  }, {
    full: `
They are the bosses for a reason. We just have to trust them.`,
    sender: TEAM_BLUE,
    text: "{<RAND>They} are {<RAND>boss} for a {<RAND>reason}"
  }, {
    full: `
I hope you are right. We can't throw away our shot at this.`,
    sender: TEAM_RED,
    text: "I {<RAND>hope} you are {<RAND>right}."
  }]
};