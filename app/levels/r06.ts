import { DAVIDSON } from "../constants/people";
import { TEAM_GREEN } from "../constants/senders";
import { DESK_CORKBOARD_REBEL, CLUE_AZZA, CLUE_CAESAR_1, CLUE_SUBST, CLUE_NUMER, CLUE_RAIL, CLUE_COLUMN } from "../constants/sprites";
import { CREDITS_STATE } from "../constants/states";

import AZZACipher from "../ciphers/AZZACipher";
import CaeserCipher from "../ciphers/CaesarCipher";
import SubstitutionCipher from "../ciphers/SubstitutionCipher";
import NumericCipher from "../ciphers/NumericCipher";
import RailFenceCipher from "../ciphers/RailFenceCipher";
import ColumnCipher from "../ciphers/ColumnCipher";

import StringKey from "../keyConfigs/StringKey";
import NumericKey from "../keyConfigs/NumericKey";
import StaticStringKey from "../keyConfigs/StaticStringKey";

const Cae1 = [[CaeserCipher, new NumericKey(1, 1)]];
const Azza = [AZZACipher];
const Subst = [[SubstitutionCipher, new StringKey(3, 9), true]];
const Rail = [[RailFenceCipher, new NumericKey(3, 6), true]];
const Col = [[ColumnCipher, new StringKey(3, 6), true]];
const Numer = [NumericCipher];

const Cae1Numer = [[CaeserCipher, new NumericKey(1, 1)], NumericCipher];
const AzzaNumer = [AZZACipher, NumericCipher];
const SubstNumer = [[SubstitutionCipher, new StringKey(3, 9), true], NumericCipher];
const RailNumer = [[RailFenceCipher, new NumericKey(3, 6), true], NumericCipher];
const ColNumer = [[ColumnCipher, new StringKey(3, 6), true], NumericCipher];

export default {
  cipherList: {
    AZZA: [Azza],
    CAE1: [Cae1],
    RAIL: [Rail],
    SUBST: [Subst],
    COL: [Col],
    RAND: [Rail, Subst, Col],
    NUMER: [Numer],
    RAILNUMER: [RailNumer],
    COLNUMER: [ColNumer],
    RANDNUMER: [RailNumer, SubstNumer, ColNumer],
    RANDONUMER: [Rail, Subst, Col, RailNumer, SubstNumer, ColNumer],
  },
  clues: [CLUE_CAESAR_1, CLUE_AZZA, CLUE_SUBST, CLUE_NUMER, CLUE_RAIL, CLUE_COLUMN],
  corkboard: DESK_CORKBOARD_REBEL,
  email: {
    content: `
Our systems are almost ready to hack in and take down Utopia. We just need to figure out where to target it.
The government is tightening their ciphers even more. We are so close. We can't fail now.`,
    sender: DAVIDSON,
    title: "So Close."
  },
  messages: [{
    full: `
We have reports of rebel activity from the east coast. Should we send scouts?`,
    sender: TEAM_GREEN,
    text: "{<RANDONUMER>Rebel activity to {<SUBST>east}}"
  }, {
    full: `
Yes. Shut them down now, before they find the backdoor.`,
    sender: TEAM_GREEN,
    text: "{<RANDONUMER>{<SUBST>Shut} them down now}"
  }],
  nextState: CREDITS_STATE
};
