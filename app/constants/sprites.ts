export const AVATAR_DAVIDSON = "AVATAR_DAVIDSON";
export const AVATAR_GLYNNE = "AVATAR_GLYNNE";
export const AVATAR_BURNELL = "AVATAR_BURNELL";
export const AVATAR_MCLURE = "AVATAR_MCLURE";
export const AVATAR_SILVERSTONE = "AVATAR_SILVERSTONE";
export const AVATAR_THOMPSON = "AVATAR_THOMPSON";
export const AVATAR_CHARLES = "AVATAR_CHARLES";
export const CLUE_AZZA = "CLUE_AZZA";
export const CLUE_CAESAR_1 = "CLUE_CAESAR_1";
export const CLUE_WORD = "CLUE_WORD";
export const CLUE_NUMBER = "CLUE_NUMBER";
export const CLUE_SUBST = "CLUE_SUBST";
export const CLUE_NUMER = "CLUE_NUMER";
export const CLUE_COLUMN = "CLUE_COLUMN";
export const CLUE_RAIL = "CLUE_RAIL";
export const CREDITS_IMAGE = "CREDITS_IMAGE";
export const DESK_BUTTON_CONTINUE = "BUTTON_CONTINUE";
export const DESK_BUTTON_EMAIL = "BUTTON_EMAIL";
export const DESK_BUTTON_NOTES = "BUTTON_NOTES";
export const DESK_CORKBOARD = "DESK_CORKBOARD";
export const DESK_CORKBOARD_REBEL = "DESK_CORKBOARD_REBEL";
export const DESK_EMAIL = "DESK_EMAIL";
export const DESK_EMAIL_ARROW = "DESK_EMAIL_ARROW";
export const DESK_EMAIL_CLOSE = "DESK_EMAIL_CLOSE";
export const DESK_GOVERNMENT = "DESK_GOVERNMENT";
export const DESK_NOTES = "DESK_NOTES";
export const DESK_PAPER = "DESK_PAPER";
export const DESK_REBELLION = "DESK_REBELLION";
export const DESK_RESULT_CORRECT = "DESK_RESULT_CORRECT";
export const DESK_RESULT_WRONG = "DESK_RESULT_WRONG";
export const DESK_TICKER = "DESK_TICKER";
export const DESK_TICKER_PAUSE = "DESK_TICKER_PAUSE";
export const DESK_TICKER_PLAY = "DESK_TICKER_PLAY";
export const FADE = "FADE";
export const LOGO_IMAGE = "LOGO_IMAGE";
export const SPLASH_IMAGE = "SPLASH_IMAGE";
export const START_CONTINUE = "START_CONTINUE";
export const START_NEW_GAME = "START_NEW_GAME";
export const START_OKAY = "START_OKAY";

export const SPRITES_LIST = {
  [AVATAR_DAVIDSON]: "avatars/davidson.png",
  [AVATAR_GLYNNE]: "avatars/glynne.png",
  [AVATAR_BURNELL]: "avatars/burnell.png",
  [AVATAR_MCLURE]: "avatars/mclure.png",
  [AVATAR_SILVERSTONE]: "avatars/silverstone.png",
  [AVATAR_THOMPSON]: "avatars/thompson.png",
  [AVATAR_CHARLES]: "avatars/charles.png",
  [CLUE_CAESAR_1]: "clues/CaesarCipher1.png",
  [CLUE_AZZA]: "clues/AZZACipher.png",
  [CLUE_SUBST]: "clues/SubstCipher.png",
  [CLUE_NUMER]: "clues/NumerCipher.png",
  [CLUE_COLUMN]: "clues/ColumnCipher.png",
  [CLUE_RAIL]: "clues/RailCipher.png",
  [CLUE_NUMBER]: "clues/NumberClue.png",
  [CLUE_WORD]: "clues/WordClue.png",
  [CREDITS_IMAGE]: "misc/credits.png",
  [DESK_BUTTON_CONTINUE]: "desk/continue.png",
  [DESK_BUTTON_EMAIL]: "desk/email_button.png",
  [DESK_BUTTON_NOTES]: "desk/notes_button.png",
  [DESK_CORKBOARD]: "desk/corkboard.png",
  [DESK_CORKBOARD_REBEL]: "desk/corkboard_rebel.png",
  [DESK_EMAIL]: "desk/email.png",
  [DESK_EMAIL_ARROW]: "desk/email_arrow.png",
  [DESK_EMAIL_CLOSE]: "desk/email_close.png",
  [DESK_GOVERNMENT]: "desk/government.png",
  [DESK_NOTES]: "desk/notes.png",
  [DESK_PAPER]: "desk/paper.png",
  [DESK_REBELLION]: "desk/rebellion.png",
  [DESK_RESULT_CORRECT]: "desk/result_correct.png",
  [DESK_RESULT_WRONG]: "desk/result_wrong.png",
  [DESK_TICKER]: "desk/ticker.png",
  [DESK_TICKER_PAUSE]: "desk/ticker_pause.png",
  [DESK_TICKER_PLAY]: "desk/ticker_play.png",
  [FADE]: "misc/fade.png",
  [LOGO_IMAGE]: "misc/logo.png",
  [SPLASH_IMAGE]: "misc/splash.png",
  [START_CONTINUE]: "start/continue.png",
  [START_NEW_GAME]: "start/new_game.png",
  [START_OKAY]: "start/okay.png"
};
export const SPRITES_PATH = "sprites/";
