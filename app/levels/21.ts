import { MCLURE } from "../constants/people";
import { TEAM_RED, TEAM_BLUE } from "../constants/senders";
import { CLUE_AZZA, CLUE_CAESAR_1, CLUE_SUBST, CLUE_NUMER } from "../constants/sprites";

import AZZACipher from "../ciphers/AZZACipher";
import CaeserCipher from "../ciphers/CaesarCipher";
import SubstitutionCipher from "../ciphers/SubstitutionCipher";
import NumericCipher from "../ciphers/NumericCipher";

import StringKey from "../keyConfigs/StringKey";
import NumericKey from "../keyConfigs/NumericKey";
import StaticStringKey from "../keyConfigs/StaticStringKey";

const Cae1 = [[CaeserCipher, new NumericKey(1, 1)]];
const Azza = [AZZACipher];
const Subst = [[SubstitutionCipher, new StringKey(3, 6), true]];
const Numer = [NumericCipher];


export default {
  cipherList: {
    RAND: [Azza, Cae1, Subst],
    NUM: [Numer],
  },
  clues: [CLUE_CAESAR_1, CLUE_AZZA, CLUE_SUBST, CLUE_NUMER],
  email: {
    content: `
Just to keep you on your toes, there appears to be a new cipher they are using. Details are on your pinboard.
Other than that, it's just business as usual.`,
    sender: MCLURE,
    title: "Another new cipher"
  },
  messages: [{
    full: `
The end is coming. Revenge is coming. Soon`,
    sender: TEAM_RED,
    text: "{<NUMER>Revenge} is {<NUMER>coming}"
  }, {
    full: `
How soon?`,
    sender: TEAM_BLUE,
    text: "{<NUMER>how soon}"
  }, {
    full: `
Give us a week and we can make our move.`,
    sender: TEAM_RED,
    text: "{<NUMER>Give} us a {<NUMER>week}"
  }]
};