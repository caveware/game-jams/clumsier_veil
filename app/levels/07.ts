import { MCLURE } from "../constants/people";
import { TEAM_RED, TEAM_BLUE } from "../constants/senders";
import { CLUE_CAESAR_1 } from "../constants/sprites";

import CaeserCipher from "../ciphers/CaesarCipher";
import NumericKey from "../keyConfigs/NumericKey";

const Cae1 = [[CaeserCipher, new NumericKey(1, 1)]];

export default {
  cipherList: {
    CAE1: [Cae1]
  },
  clues: [CLUE_CAESAR_1],
  email: {
    content: `
I knew you were the right person. You've had no troubles at all with these messages. Keep it up.`,
    sender: MCLURE,
    title: "Keep it up"
  },
  messages: [{
    full: `
I may have an idea... We used to have a base of operations to the south of the capital, until we got flushed out. That could work.`,
    sender: TEAM_RED,
    text: "I may have an {<CAE1>idea}"
  }]
};