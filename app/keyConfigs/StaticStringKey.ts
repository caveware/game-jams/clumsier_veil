import KeyConfig from "../interfaces/KeyConfig";
import StringKey from "./StringKey";

export default class StaticStringKey extends StringKey {

  constructor (public keyword: string) {
    super(0, 0);
  }

  generateKey() {
    return this.keyword.toUpperCase();
  }

}