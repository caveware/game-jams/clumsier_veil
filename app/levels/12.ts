import { MCLURE } from "../constants/people";
import { TEAM_RED, TEAM_BLUE } from "../constants/senders";
import { CLUE_CAESAR_1, CLUE_AZZA } from "../constants/sprites";

import CaeserCipher from "../ciphers/CaesarCipher";
import NumericKey from "../keyConfigs/NumericKey";
import AZZACipher from "../ciphers/AZZACipher";

const Cae1 = [[CaeserCipher, new NumericKey(1, 1)]];
const Azza = [AZZACipher];

export default {
  cipherList: {
    RAND: [Cae1, Azza],
  },
  clues: [CLUE_CAESAR_1, CLUE_AZZA],
  messages: [{
    full: `
What happened to careful? Not being followed? What if we hadn't set advance scouts?`,
    sender: TEAM_BLUE,
    text: "so much for {<RAND>Careful>}"
  }, {
    full: `
Us? We were careful, just as we always are. You screwed it up, not us.`,
    sender: TEAM_RED,
    text: "we were {<RAND>Careful>}"
  }]
};