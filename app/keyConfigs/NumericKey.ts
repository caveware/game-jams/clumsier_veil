import KeyConfig from "../interfaces/KeyConfig";

export default class NumericKey implements KeyConfig {

  keyType = "number";

  constructor (public minBound: number, public maxBound: number) {
    if (minBound > maxBound) {
      let tmp = minBound;
      minBound = maxBound;
      maxBound = tmp;
    }
  }

  generateKey() {
    return Math.floor(Math.random() * (this.maxBound + 1 - this.minBound) + this.minBound);
  }

}